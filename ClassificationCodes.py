import numpy as np
import matplotlib.pyplot as plt
import scipy.io as sio
from brainpipe.classification import *
from brainpipe.visual import *
import mne
from mne.viz import plot_topomap
from mne import pick_types, find_layout
from scipy import stats
from random import sample
import os


def load_PSD_data(data_path,subject_name,cote,session,bande,norm):
    data_file = '{s}_{c}_{ses}_bp_PSD.mat'
    ck = sio.loadmat(os.path.join(data_path,data_file.format(s=subject_name,c=cote,ses=session)))

    if bande=='Delta':
        mat=ck['x'].conj().T
        Px=np.squeeze(mat[:,:,0])
    elif bande=='Theta':
        mat=ck['x'].conj().T
        Px=np.squeeze(mat[:,:,1])
    elif bande=='Alpha':
        mat=ck['x'].conj().T
        Px=np.squeeze(mat[:,:,2])

    elif bande=='Beta':
        mat=ck['x'].conj().T
        Px=np.squeeze(mat[:,:,3])
    elif bande=='Gamma1':
        mat=ck['x'].conj().T
        Px=np.squeeze(mat[:,:,4])
    elif bande=='Gamma2':
        mat=ck['x'].conj().T
        Px=np.squeeze(mat[:,:,5])
    if norm==1:
        return np.array(stats.zscore(Px,axis=1))
    else:
        return np.array(Px)
def Da_bino(y,p):
    y = np.ravel(y)
    nbsamples = len(y)
    nbclass = len(np.unique(y))
    da_bino=stats.binom.ppf(1 - p, nbsamples, 1 / nbclass) * 100 / nbsamples
    return da_bino

def Topoplot_DA(DA=None,sensors_pos=None,masked=False,chance_level=0.05,Da_perm=None,Da_bino=None,figures_save_file=None,show_fig=False):
    if masked==True:
        mask_default = np.full((len(DA)), False, dtype=bool)
        mask = np.array(mask_default)
        if Da_perm is not None:
            nperm=Da_perm.shape[0]
            indx_chance=int(nperm-chance_level*nperm)
            daperm=np.sort(Da_perm,axis=0)
            mask[DA>=np.amax(daperm[indx_chance-2,:])] = True
        if Da_bino is not None:
            mask[DA>=Da_bino] = True



        mask_params = dict(marker='*', markerfacecolor='w', markersize=15)
        fig1 = plt.figure(figsize = (10,15))
        ax1,_ = plot_topomap(DA, sensors_pos,
                             cmap='seismic', show=show_fig,
                             vmin=0, vmax=100,
                             mask = mask,
                             mask_params = mask_params)
                             #names=elect,show_names=True)
    elif masked==False:


        fig1 = plt.figure(figsize = (10,15))
        ax1,_ = plot_topomap(DA, sensors_pos,
                             cmap='seismic', show=show_fig,
                             vmin=0, vmax=100)
    #add colorbar
    fig1.colorbar(ax1, shrink=0.25)
    #save figure to tif file
    if figures_save_file is not None:
        plt.savefig(figures_save_file, dpi = 300)
