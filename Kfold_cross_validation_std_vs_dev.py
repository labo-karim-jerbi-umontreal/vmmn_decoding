
# coding: utf-8

# # Import  libraries

# In[1]:

import numpy as np
import scipy.io as sio
from brainpipe.classification import *
from brainpipe.visual import *
import matplotlib.pyplot as plt
from random import sample
import mne
from mne.viz import plot_topomap
from mne import pick_types, find_layout
from scipy import stats
from ClassificationCodes import load_PSD_data,Da_bino,Topoplot_DA
import os
main_path=os.getcwd()

data_path='/home/karim/Vanessa/PSD/' #path to your PSD data

save_path=os.path.join(main_path,'Classification')
if not os.path.exists(save_path):
    os.makedirs(save_path)

subject_list=['Lisa'] #subject name
cote='Droite' # 'Gauche , Droite
session1='Std'
session2='Dev'
bande='Alpha' #frequency band
classifier='SVM' #classifier name
K=10
norm=1
nb_rep=3 #number of iteration in boostraping (100)

sensors_pos = sensors_pos = sio.loadmat('/home/karim/Vanessa/coord_elect_vanessa.mat')['chxy']
elect =['Fp1','Fp2','F7', 'F3', 'Fz', 'F4','F8', 'FT7', 'FC3',
                  'FCz','FC4','FT8' ,'T3','C3','Cz','C4', 'T4','TP7','CP3','CPz' ,
                  'CP4', 'TP8','A1','T5','P3','Pz','P4','T6','A2','O1',
                  'Oz','O2','FT9','FT10','PO7','PO8']
for subject_name in subject_list:
    ############################Loading Powers basing on parametres set above #########################################
    Px_st=load_PSD_data(data_path,subject_name,cote,session1,bande,norm) #Load Standard Power
    Px_dv=load_PSD_data(data_path,subject_name,cote,session2,bande,norm) #load deviant Power
    ##################################Select random number of observations to get balanced data ######################
    nstd=Px_st.shape[0]
    ndev=Px_dv.shape[0]
    DA=np.array([])
    print('Running {} fold classification for subject, {} in {} bande and {} side!'.format(K,subject_name,bande,cote))
    print('{} samples are randomly selected in each classe'.format(ndev))
    for itr in range(nb_rep):
        print('Split number {}'.format(itr+1))

        if ndev>nstd:
            indx=sample(range(ndev),nstd)
            Px_dev=np.copy(Px_dv[indx,:])
            Px_std=np.copy(Px_st)
        elif nstd>ndev:
            indx=sample(range(nstd),ndev)
            Px_std=np.copy(Px_st[indx,:])
            Px_dev=np.copy(Px_dv)
        else:
            Px_dev=np.copy(Px_dv)
            Px_std=np.copy(Px_st)
    ###############################################Classification##########################################
        x_data=np.vstack([Px_std,Px_dev])

        Y=np.array([1]*Px_std.shape[0] + [0]*Px_dev.shape[0])
        dabino05=Da_bino(Y,0.05)
        dabino01=Da_bino(Y,0.01)
        dabino001=Da_bino(Y,0.001)

        cv = defCv(Y, cvtype='kfold',n_folds=K,rep=1)
        clf = defClf(Y, clf=classifier)
        clfObj= classify(Y, clf=clf, cvtype=cv)
        da,pvalues,da_bino= clfObj.fit(x_data, mf=False, grp=None,method='bino', rndstate=0, n_jobs=-1)
        DA=np.concatenate((DA,da),axis=0) if DA.size else da

    save_file=os.path.join(save_path,'DA_bootstraping_{subj}_{b}_{cot}_{cl}_{k}fold'.format(subj=subject_name,
                                                                                       b=bande,
                                                                                       cot=cote,
                                                                                       cl=classifier,
                                                                                       k=str(K)))

    sio.savemat(save_file,{'DA':DA,'dabino05':Da_bino,'dabino01':dabino01,'dabino001':dabino001})

    del Px_st,Px_dv



fig_file =os.path.join(save_path,'Topo_{subj}_{b}_{cot}_{cl}_{k}fold.tif'.format(subj=subject_name,
                                                                                 b=bande,
                                                                                 cot=cote,
                                                                                 cl=classifier,
                                                                                 k=str(K)))

Topoplot_DA(DA=np.mean(DA,axis=0),sensors_pos=sensors_pos,masked=True,chance_level=0.05,Da_bino=dabino05,figures_save_file=fig_file)
print('Figures and Decoding Accuracies are saved in ',save_path)
